# chinerlamachine

Oui <br>
Voila, c'est tout ce qu'il y a à savoir sur ce projet. <br>
Oui je vais apprendre des choses, oui c'est purement pédagogique, oui ce n'est pas optimisé, oui il y a autant de commentaire que de code. <br>

## Et encore ?
C'est quoi une moyenne ? La somme de toutes les valeurs divisé par le nombre de valeurs. <br>
C'est quoi une somme ? Une valeur ? <br>
C'est quoi une Analyse des composantes principales ? <br>
C'est quoi un réseau de neurones convolutif ? <br>
C'est quoi .... <br>

Voila l'idée de ce package, répondre aux questions sans faire de raccourcis, et le tout sans être pompeux. <br>
En complément d'images / gif / vidéos pour compléter les explications. 

# Pour moi, pour toi, pour tous ceux qui le veulent. 🎤
Allez viens, entre dans le code 🎵 <br>
On va apprendre ensemble <br>
Pour comprendre le monde des diodes <br>
La voix qui tremble <br>
Ou au contraire <br>
Chosi l'itinéraire <br>
Du code machine, ou celui des rimes 🎺 <br>

# Sérinonsité
On réinvente la roue, et en faisant ça, on sait faire une roue soit même 🦄<br>

Signé : moi même. Lecteur : toi même.